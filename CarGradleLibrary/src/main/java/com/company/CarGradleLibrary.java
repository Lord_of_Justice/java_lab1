package com.company;

public class CarGradleLibrary {
    private static boolean IsEngineRunning;

    public CarGradleLibrary(boolean engineState){
        IsEngineRunning = engineState;
    }
    public static boolean IsCarRunning(){
        return IsEngineRunning;
    }
}

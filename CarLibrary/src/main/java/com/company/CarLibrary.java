package com.company;

public class CarLibrary {
    private static boolean isEngineRunning;

    public CarLibrary(boolean engineState){
        isEngineRunning = engineState;
    }
    public static boolean IsRunning(){
        return isEngineRunning;
    }
}

package com.company;

public class CarGradleUsing {
    public static void main(String[] args) {
        CarGradleLibrary car = new CarGradleLibrary(true);
        if (car.IsCarRunning())
            System.out.println("Our car is running");
        else
            System.out.println("Our car is not running! Switch on your engine!");
    }
}
